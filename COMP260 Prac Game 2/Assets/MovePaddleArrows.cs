﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddleArrows : MonoBehaviour {

	private Rigidbody rigidbody;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}
		
	public float force = 1f;

	void FixedUpdate () {
		if(Input.GetAxis("Horizontal") > 0)
			rigidbody.AddForce (force, 0, 0, ForceMode.Impulse);
		if(Input.GetAxis("Horizontal") < 0)
			rigidbody.AddForce (-force, 0, 0, ForceMode.Impulse);
		if(Input.GetAxis("Vertical") > 0)
			rigidbody.AddForce (0, force, 0, ForceMode.Impulse);
		if(Input.GetAxis("Vertical") < 0)
			rigidbody.AddForce (0, -force, 0, ForceMode.Impulse);

		rigidbody.AddForce (-rigidbody.velocity * 10.0f);
	}
}
