﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class PaddleAI : MonoBehaviour {

	public Transform puck;
	public Transform goal;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public float speed = 20f;

	void FixedUpdate () {
		Vector3 pos = (puck.position + goal.position)/2;
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	}
}